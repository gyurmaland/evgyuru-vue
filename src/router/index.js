import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import ProjectDetailsView from '@/views/project-details/ProjectDetailsView.vue';
import FinancialStatementsView from '@/views/documents/FinancialStatementsView.vue';
import CourseDetailsView from '@/views/course-details/CourseDetailsView.vue';

const routes = [
  {
    path: '/',
    name: 'homeView',
    component: HomeView,
  },
  {
    path: '/project/:projectId',
    name: 'projectDetailsView',
    component: ProjectDetailsView,
  },
  {
    path: '/course/:courseId',
    name: 'courseDetailsView',
    component: CourseDetailsView,
  },
  {
    path: '/documents/financial-statements',
    name: 'financialStatementsView',
    component: FinancialStatementsView,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  // eslint-disable-next-line consistent-return,no-unused-vars
  scrollBehavior(to, from, savedPositions) {
    if (to.hash) {
      return {
        el: to.hash,
        behavior: 'smooth',
        left: 0,
        top: 0,
      };
    }
  },
});

export default router;
