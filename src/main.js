import { createApp } from 'vue';
import VueSocialSharing from 'vue-social-sharing';
import { createMetaManager, plugin as metaPlugin } from 'vue-meta';
import i18n from '@/locales/i18n';
import ListLoadingAnimation from '@/components/common/ListLoadingAnimation.vue';
import App from './App.vue';
import router from './router';
import '@/assets/styles/main.css';

createApp(App)
  .use(VueSocialSharing)
  .use(createMetaManager())
  .use(metaPlugin)
  .use(router)
  .use(i18n)
  .component('listLoadingAnimation', ListLoadingAnimation)
  .mount('#app');
